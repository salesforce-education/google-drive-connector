/**
 * Created by Andrii Kniaziev on 9/28/2022.
 */

({
    doInit: function(component) {
        const actions = [
            { label: 'Download', name: 'download' },
            { label: 'Delete', name: 'delete' }
        ];
        component.set('v.columns', [
            {label: 'Name', fieldName: 'name', type: 'text'},
            {label: 'Type', fieldName: 'mimeType', type: 'text'},
            {label: 'In trash', fieldName: 'trashed', type: 'text'},
            {type:  'action', typeAttributes: { rowActions: actions }},
        ]);
    },

    handleRowAction: function(component, event, helper) {
        let action = event.getParam('action');
        let row = event.getParam('row');
        switch (action.name) {
            case 'download':
                helper.downloadFile(component, row);
                break;
            case 'delete':
                helper.deleteFile(component, row);
                break;
        }
    }
});