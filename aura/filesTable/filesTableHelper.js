/**
 * Created by Andrii Kniaziev on 9/28/2022.
 */

({
    downloadFile: function(component, file) {
        const eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
            "url": file.webViewLink
        });
        eUrl.fire();
    },

    deleteFile: function(component, file) {
        let deleteFile = component.get("c.deleteFile");

        deleteFile.setParams({
            connectorType: component.get("v.fileStorage"),
            fileId: file.id
        });
        deleteFile.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() === '204') {
                    this.handleShowToast(component, 'Success', 'File ' + file.name + ' was deleted.', 'success');
                    component.getEvent("updateFileList").fire();
                }
            } else {
                this.handleShowToast(component, 'Error', response.getReturnValue(), 'error');
            }
        });

        $A.enqueueAction(deleteFile);
    },

    handleShowToast : function(component, title, message, variant) {
        component.find('notifLib').showToast({
            "title": title,
            "message": message,
            "variant": variant
        });
    }
});