/**
 * Created by Andrii Kniaziev on 9/30/2022.
 */

({
    MAX_FILE_SIZE: 4500000,  // Max file size 4.5 MB
    CHUNK_SIZE: 750000,      // Chunk Max size 750Kb

    uploadHelper: function(component, event) {
        let fileInput = component.find("fuploader").get("v.files");

        let file = fileInput[0];
        let self = this;

        if (file.size > self.MAX_FILE_SIZE) {
            this.handleShowToast(
                component,
                'Warning',
                'File size cannot exceed ' +
                self.MAX_FILE_SIZE +
                ' bytes.\n' +
                ' Selected file size: ' + file.size,
                'warning'
            );
            return;
        }

        let objFileReader = new FileReader();

        objFileReader.onload = $A.getCallback(function() {
            let fileContents = objFileReader.result;
            let base64 = 'base64,';
            let dataStart = fileContents.indexOf(base64) + base64.length;

            fileContents = fileContents.substring(dataStart);
            self.uploadProcess(component, file, fileContents);
        });

        objFileReader.readAsDataURL(file);
    },

    uploadProcess: function(component, file, fileContents) {
        let startPosition = 0;
        let endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);

        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
    },


    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        let getChunk = fileContents.substring(startPosition, endPosition);
        let action = component.get("c.saveFile");

        action.setParams({
            connectorType: component.get("v.fileStorage"),
            fileName: file.name,
            base64Data: encodeURIComponent(getChunk),
            contentType: file.type,
        });
        action.setCallback(this, function(response) {
            attachId = response.getReturnValue();
            let state = response.getState();
            if (state === "SUCCESS") {
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);

                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                } else {
                    component.getEvent("updateFileList").fire();
                    this.handleShowToast(
                        component,
                        'Success',
                        'File ' + component.get("v.fileName") + ' was uploaded successfully.',
                        'success'
                    );
                }
            } else if (state === "INCOMPLETE") {
                this.handleShowToast(component, 'Incomplete', "From server: " + response.getReturnValue(), 'warning');
            } else if (state === "ERROR") {
                this.handleShowToast(component, 'Error', response.getReturnValue(), 'error');
            }
        });

        $A.enqueueAction(action);
    },

    handleShowToast : function(component, title, message, variant) {
        component.find('notifLib').showToast({
            "title": title,
            "message": message,
            "variant": variant
        });
    }
});