/**
 * Created by User on 22.07.2022.
 */

({
    doInit: function(component, event, helper) {
        function getUrlParamValue(url, key) {
            return new URL(url).searchParams.get(key);
        }
        const code = getUrlParamValue(window.location.href, 'namespace__code');
        if (code) {
            helper.requestAccessToken(component, code);
        }
        component.set("v.connectorType", component.get("v.fileStorage"));
        helper.getFilesList(component);
    },

    handleMakeAuth: function(component, event, helper) {
        helper.performAuth(component);
    },

    handleGetFilesList: function(component, event, helper) {
        helper.getFilesList(component);
    },
});