/**
 * Created by User on 22.07.2022.
 */

({
    performAuth: function(component) {
        let generateAuthUri = component.get("c.generateAuthUri");

        generateAuthUri.setParams({connectorType: component.get("v.fileStorage")});
        generateAuthUri.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                this.redirectToUrl(response.getReturnValue());
            } else {
                this.handleShowToast(component, 'Error', response.getReturnValue(), 'error');
            }
        });

        $A.enqueueAction(generateAuthUri);
    },

    redirectToUrl: function(url) {
        const eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
            "url": url
        });
        eUrl.fire();
    },

    requestAccessToken: function(component, authorizationCode) {
        let getAccessToken = component.get("c.getAccessToken");

        getAccessToken.setParams({
            connectorType: component.get("v.fileStorage"),
            authorizationCode: authorizationCode
        });
        getAccessToken.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                this.getFilesList(component);
            } else {
                this.handleShowToast(component, 'Error', response.getReturnValue(), 'error');
            }
        });

        $A.enqueueAction(getAccessToken);
    },

    getFilesList: function(component) {
        let getFilesList = component.get("c.getFilesList");

        getFilesList.setParams({connectorType: component.get("v.fileStorage")});
        getFilesList.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                const result = JSON.parse(response.getReturnValue());

                if (result.files || result.files.length) {
                    component.set("v.isAuthenticated", true);
                    result.files.forEach(file => {
                        if (file.trashed) {
                            file.trashed = 'Yes';
                        } else {
                            file.trashed = '';
                        }
                    });
                    component.set("v.files", result.files);
                } else {
                    component.set("v.isAuthenticated", false);
                }
            } else {
                this.handleShowToast(component, 'Error', response.getReturnValue(), 'error');
            }
        });

        $A.enqueueAction(getFilesList);
    },

    handleShowToast : function(component, title, message, variant) {
        component.find('notifLib').showToast({
            "title": title,
            "message": message,
            "variant": variant
        });
    }
});