/**
 * Created by User on 22.07.2022.
 */

public with sharing class GoogleDriveController {

    @AuraEnabled
    public static String generateAuthUri(String connectorType) {
        GoogleDriveInfo__c info = GoogleDriveInfo__c.getInstance(UserInfo.getUserId());
        Connector connector = ConnectorFactory.getConnector(connectorType);
        try {
            return connector.generateAuthUri(info.Client_Id__c, info.Redirect_URI__c);
        } catch (Exception e) {
            throw new VisualforceException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void getAccessToken(String connectorType, String authorizationCode) {
        Connector connector = ConnectorFactory.getConnector(connectorType);
        try {
            connector.getAccessToken(authorizationCode);
        } catch (Exception e) {
            throw new VisualforceException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String getFilesList(String connectorType) {
        Connector connector = ConnectorFactory.getConnector(connectorType);
        try {
            return connector.getFileList();
        } catch (Exception e) {
            throw new VisualforceException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String deleteFile(String connectorType, String fileId) {
        Connector connector = ConnectorFactory.getConnector(connectorType);
        try {
            return connector.deleteFile(fileId);
        } catch (Exception e) {
            throw new VisualforceException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String saveFile(
            String connectorType,
            String fileName,
            String base64Data,
            String contentType
    ) {
        Connector connector = ConnectorFactory.getConnector(connectorType);
        try {
            return connector.uploadFile(base64Data, fileName, contentType);
        } catch (Exception e) {
            throw new VisualforceException(e.getMessage());
        }
    }
}