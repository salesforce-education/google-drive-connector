/**
 * Created by Andrii Kniaziev on 10/4/2022.
 */

public with sharing class ConnectorFactory {

    public static Connector getConnector(String connectorType) {
        if (connectorType.equalsIgnoreCase('google drive')) {
            return new GoogleDriveConnector();
        } else {
            return null;
        }
    }
}