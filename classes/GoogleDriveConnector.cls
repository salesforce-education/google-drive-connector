/**
 * Created by User on 22.07.2022.
 */

public with sharing class GoogleDriveConnector implements Connector {

    private GoogleDriveInfo__c info = GoogleDriveInfo__c.getInstance(UserInfo.getUserId());

    private static final String GET_METHOD = 'GET';
    private static final String POST_METHOD = 'POST';
    private static final String DELETE_METHOD = 'DELETE';

    private static final String BOUNDARY = '----------9889464542212';
    private static final String DELIMITER = '\r\n--' + BOUNDARY +'\r\n';
    private static final String CLOSE_DELIMITER = '\r\n--' + BOUNDARY + '--';

    public String generateAuthUri(
            String clientId,
            String redirectUri
    ) {
        String encodedClientId = EncodingUtil.urlEncode(clientId, 'UTF-8');
        String encodedRedirectUri = EncodingUtil.urlEncode(redirectUri, 'UTF-8');

        return  'https://accounts.google.com/o/oauth2/v2/auth?' +
                'client_id=' + encodedClientId +
                '&redirect_uri=' + encodedRedirectUri +
                '&response_type=code' +
                '&scope=' +
                'https://www.googleapis.com/auth/drive ' +
                'https://www.googleapis.com/auth/drive.file' +
                '&access_type=offline';
    }

    public void getAccessToken(String code) {
        String encodedCode = EncodingUtil.urlEncode(code, 'UTF-8');
        HttpRequest request = new HttpRequest();

        request.setMethod(POST_METHOD);
        request.setEndpoint('https://oauth2.googleapis.com/token');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');

        String messageBody =
                'client_id=' + info.Client_Id__c +
                '&client_secret=' + info.Client_Secret__c +
                '&code=' + encodedCode +
                '&grant_type=authorization_code' +
                '&redirect_uri=' + info.Redirect_URI__c;
        request.setHeader('Content-length', String.valueOf(messageBody.length()));
        request.setBody(messageBody);
        request.setTimeout(60 * 1000);

        Http http = new Http();
        HttpResponse response = http.send(request);

        JSONParser parser = JSON.createParser(response.getBody());
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'access_token' || parser.getText() == 'refresh_token')) {
                if (parser.getText() == 'access_token') {
                    parser.nextToken();
                    info.AccessToken__c = parser.getText();
                } else {
                    parser.nextToken();
                    info.RefreshToken__c = parser.getText();
                }
            }
        }
        update info;
    }

    public String getFileList() {
        HttpRequest request = new HttpRequest();

        request.setMethod(GET_METHOD);
        request.setEndpoint('https://www.googleapis.com/drive/v3/files?fields=files(id,name,createdTime,mimeType,webViewLink,trashed)');
        request.setHeader('content-type', 'application/x-www-form-urlencoded');
        request.setHeader('Content-length', '0');
        request.setHeader('Authorization', 'Bearer' + ' ' + info.AccessToken__c);
        request.setTimeout(60 * 1000);

        Http http = new Http();
        HttpResponse response = http.send(request);

        return response.getBody();
    }

    public String deleteFile(String fileId) {
        HttpRequest request = new HttpRequest();

        request.setMethod(DELETE_METHOD);
        request.setEndpoint('https://www.googleapis.com/drive/v3/files/' + fileId);
        request.setHeader('Authorization', 'Bearer' + ' ' + info.AccessToken__c);
        request.setTimeout(60 * 1000);

        Http http = new Http();
        HttpResponse response = http.send(request);

        return String.valueOf(response.getStatusCode());
    }

    public String uploadFile(String base64, String filename, String contentType) {
        base64 = EncodingUtil.urlDecode(base64, 'UTF-8');

        String body = DELIMITER +
                'Content-Type: application/json\r\n\r\n' +
                '{ "title" : "' + filename +
                '",'+' "mimeType" : "'+ contentType +
                '" }' + DELIMITER +
                'Content-Type: ' + contentType + '\r\n' +
                'Content-Transfer-Encoding: base64\r\n' +
                '\r\n' + base64 + CLOSE_DELIMITER;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart');
        request.setHeader('Authorization', 'Bearer ' + info.AccessToken__c);
        request.setHeader('Content-Type', 'multipart/mixed; boundary="'+ BOUNDARY +'"');
        request.setHeader('Content-length', String.valueOf(body.length()));
        request.setBody(body);
        request.setMethod(POST_METHOD);
        request.setTimeout(60 * 1000);
        HttpResponse response = http.send(request);

        return String.valueOf(response.getStatusCode());
    }

    private class UnsupportedFormatException extends Exception {}
}