/**
 * Created by Andrii Kniaziev on 10/3/2022.
 */

public interface Connector {
    String generateAuthUri(String clientId, String redirectUri);
    void getAccessToken(String code);
    String getFileList();
    String uploadFile(String base64, String filename, String contentType);
    String deleteFile(String fileId);
}